# S3 Backup

This tool syncs new files from a folder into S3 (Standard or Glacier).


## Prereqs
* NodeJS (tested on v16.15.0)
* AWS credentials in the default INI file
* A private S3 bucket
  * Recommend a lifecycle policy for cancelled multipart uploads
  * Recommend object locks


## What it does
1. Looks for new files added to the source directory.
2. Puts all these new files into a zip folder using relative paths.
3. Uploads the zip folder to S3 (using multipart for resume).


## Sample config file
You can pass command line arguments in directly or create a config file and use the `--config` option.
```
{
  "targetDir": "C:\\Users\\someone\\backup",
  "sourceDir": "C:\\Users\\someone\\archivespath",
  "bucketName": "s3-bucket-name",
  "bucketRegion": "us-east-1",
  "bucketStorageClass": "STANDARD"
}
```

