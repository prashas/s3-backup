import { mockClient } from 'aws-sdk-client-mock';
import {
  NoSuchKey,
  S3Client,
  ListMultipartUploadsCommand,
  GetObjectCommand,
  CreateMultipartUploadCommand,
  ListPartsCommand,
  UploadPartCommand,
  CompleteMultipartUploadCommand,
} from '@aws-sdk/client-s3';
import { doesObjectExist, uploadToGlacier } from './s3';
import * as fsPromisesModule from 'fs/promises';

jest.mock('./config', () => ({
  UPLOAD_PART_SIZE: 4,
  userConfig: {
    bucketName: 'test-bucket',
    bucketRegion: 'local',
    bucketStorageClass: 'STANDARD',
  },
  savedState: {
    lastUpdateTime: 0,
  },
}));

jest.mock('fs/promises', () => ({
  open: jest.fn().mockRejectedValue(new Error('Not implemented.')),
}));

const s3Mock = mockClient(S3Client);

beforeEach(() => {
  s3Mock.reset();
});

describe('doesObjectExist()', () => {
  it('should return true if the object exists', async () => {
    s3Mock.on(GetObjectCommand).resolves({});
    await expect(doesObjectExist('somekey.zip')).resolves.toBe(true);
  });

  it('should return false if the 404 error is thrown', async () => {
    s3Mock.on(GetObjectCommand).rejects(new NoSuchKey({ $metadata: {} }));
    await expect(doesObjectExist('somekey.zip')).resolves.toBe(false);
  });

  it('should let other errors through', async () => {
    s3Mock.on(GetObjectCommand).rejects(new Error('Some test error'));
    await expect(doesObjectExist('somekey.zip')).rejects.toThrowError();
  });
});

async function getCommandArgs(command: Parameters<typeof s3Mock.commandCalls>[0]) {
  return (await s3Mock.commandCalls(command))[0].firstArg.input;
}

describe('uploadToGlacier()', () => {
  describe('creating the multipart upload', () => {
    it('should be able to start a new upload', async () => {
      s3Mock.on(ListMultipartUploadsCommand).resolvesOnce({ Uploads: [] });
      s3Mock.on(CreateMultipartUploadCommand).resolvesOnce({ UploadId: 'TestId' });
      await expect(uploadToGlacier('notafile.zip', 'testkey.zip')).rejects.toThrowError('Not implemented.');
      expect(await getCommandArgs(CreateMultipartUploadCommand)).toEqual({
        Bucket: 'test-bucket',
        Key: 'testkey.zip',
        ContentType: 'application/zip',
        StorageClass: 'STANDARD',
      });
    });

    it('should be able to resume an existing upload with parts', async () => {
      s3Mock.on(ListMultipartUploadsCommand).resolvesOnce({ Uploads: [{ UploadId: 'ExistingId' }] });
      s3Mock.on(ListPartsCommand).resolvesOnce({
        Parts: [
          { PartNumber: 1, ETag: 'part1' },
          { PartNumber: 2, ETag: 'part2' },
        ],
      });
      await expect(uploadToGlacier('notafile.zip', 'testkey.zip')).rejects.toThrowError('Not implemented.');
      expect(await getCommandArgs(ListPartsCommand)).toEqual({
        Bucket: 'test-bucket',
        Key: 'testkey.zip',
        UploadId: 'ExistingId',
      });
    });
  });

  describe('uploading the data', () => {
    const mockFileHandle = {
      read: jest.fn(async (loc: Buffer, _o, size) => {
        loc.set(Buffer.from(Array(size).fill(1)));
        return { bytesRead: size, buffer: loc };
      }),
      stat: jest.fn(async () => ({ size: 10 })),
    };

    beforeAll(async () => {
      jest.spyOn(fsPromisesModule, 'open').mockResolvedValue(mockFileHandle as unknown as fsPromisesModule.FileHandle);
    });

    beforeEach(() => {
      s3Mock.on(ListMultipartUploadsCommand).resolvesOnce({ Uploads: [] });
      s3Mock.on(CreateMultipartUploadCommand).resolvesOnce({ UploadId: 'TestId' });
      s3Mock.on(UploadPartCommand).resolves({ ETag: 'repeatvalue' });
    });

    it('should be able to upload a file in parts', async () => {
      await expect(uploadToGlacier('notafile.zip', 'testkey.zip')).resolves.toBeUndefined();
      expect(await s3Mock.commandCalls(UploadPartCommand)).toHaveLength(Math.ceil(10 / 4));
      expect(await getCommandArgs(CompleteMultipartUploadCommand)).toEqual({
        Bucket: 'test-bucket',
        Key: 'testkey.zip',
        UploadId: 'TestId',
        MultipartUpload: {
          Parts: expect.arrayContaining([1, 2, 3].map((i) => ({ PartNumber: i, ETag: 'repeatvalue' }))),
        },
      });
    });
  });
});
