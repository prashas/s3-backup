import {
  S3Client,
  GetObjectCommand,
  NoSuchKey,
  Part,
  ListMultipartUploadsCommand,
  paginateListParts,
  CreateMultipartUploadCommand,
  UploadPartCommand,
  CompleteMultipartUploadCommand,
} from '@aws-sdk/client-s3';
import { fromIni } from '@aws-sdk/credential-provider-ini';
import * as fs from 'fs/promises';
import { userConfig, UPLOAD_PART_SIZE } from './config';
import { startProgressBar, stopProgressBar, updateProgressWithUnit } from './logger';

let s3Client: S3Client;
function getS3Client() {
  s3Client = s3Client ?? new S3Client({ credentials: fromIni(), region: userConfig.bucketRegion });
  return s3Client;
}

export async function doesObjectExist(s3Key: string): Promise<boolean> {
  const client = getS3Client();
  try {
    await client.send(new GetObjectCommand({ Bucket: userConfig.bucketName, Key: s3Key }));
    return true;
  } catch (e) {
    if (e instanceof NoSuchKey) {
      return false;
    }
    throw e;
  }
}

async function getMultipartUpload(archiveName: string): Promise<{ uploadId: string; existingParts: Part[] }> {
  const client = getS3Client();

  let uploadId: string | undefined;
  const existingParts: Part[] = [];

  const existingUploadRes = await client.send(new ListMultipartUploadsCommand({ Bucket: userConfig.bucketName, Prefix: archiveName }));
  if (existingUploadRes.Uploads?.length) {
    const existingUpload = existingUploadRes.Uploads[0];
    uploadId = existingUpload.UploadId;
    const listPartsPaginator = paginateListParts({ client }, { Bucket: userConfig.bucketName, Key: archiveName, UploadId: uploadId });
    for await (const listPartRes of listPartsPaginator) {
      existingParts.push(...(listPartRes?.Parts ?? []));
      if (!listPartRes.IsTruncated) {
        break;
      }
    }
    console.log(`Resuming existing upload from ${existingUpload.Initiated?.toISOString()} with ${existingParts.length} parts.`);
  } else {
    const createUploadRes = await client.send(
      new CreateMultipartUploadCommand({
        Bucket: userConfig.bucketName,
        Key: archiveName,
        ContentType: 'application/zip',
        StorageClass: userConfig.bucketStorageClass,
      }),
    );
    uploadId = createUploadRes.UploadId;
    console.log(`Created new upload with id: ${uploadId}`);
  }

  if (!uploadId) {
    throw Error('Failed to create multipart upload.');
  }

  return { uploadId, existingParts };
}

export async function uploadToGlacier(archivePath: string, s3Key: string) {
  console.log(`Uploading to S3 using ${userConfig.bucketStorageClass} storage class`);
  const client = getS3Client();
  const { uploadId, existingParts } = await getMultipartUpload(s3Key);

  const commonUploadArgs = { Bucket: userConfig.bucketName, Key: s3Key, UploadId: uploadId } as const;

  const fd = await fs.open(archivePath, 'r');
  const archiveInfo = await fd.stat();
  const archiveSize = archiveInfo.size;
  const archiveNumParts = Math.ceil(archiveSize / UPLOAD_PART_SIZE);
  const archiveSizeMb = Math.ceil(archiveSize / 1024 / 1024);

  try {
    const buffer = Buffer.allocUnsafe(UPLOAD_PART_SIZE);
    startProgressBar(archiveNumParts, existingParts.length);
    for (let partNum = existingParts.length + 1; partNum <= archiveNumParts; partNum++) {
      const readRes = await fd.read(buffer, 0, UPLOAD_PART_SIZE, (partNum - 1) * UPLOAD_PART_SIZE);
      const uploadRes = await client.send(
        new UploadPartCommand({ ...commonUploadArgs, PartNumber: partNum, Body: buffer.subarray(0, readRes.bytesRead) }),
      );
      existingParts.push({ PartNumber: partNum, ETag: uploadRes.ETag });
      updateProgressWithUnit(partNum, archiveSizeMb, 'MB');
    }
  } finally {
    stopProgressBar();
  }

  console.log(`Completing upload with ${existingParts.length} parts`);
  await client.send(new CompleteMultipartUploadCommand({ ...commonUploadArgs, MultipartUpload: { Parts: existingParts } }));
  console.log(`Completed upload to "${s3Key}"`);
}
