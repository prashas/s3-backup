import * as fs from 'fs/promises';
import * as path from 'path';
import * as crypto from 'crypto';
import { createWriteStream, existsSync } from 'fs';
import archiver from 'archiver';
import { savedState, userConfig } from './config';
import { startProgressBar, stopProgressBar, updateProgressBar } from './logger';

async function* getFiles(dir: string): AsyncIterableIterator<string> {
  const dirents = await fs.readdir(dir, { withFileTypes: true });
  for (const dirent of dirents) {
    const res = path.resolve(dir, dirent.name);
    if (dirent.isDirectory()) {
      yield* getFiles(res);
    } else {
      yield res;
    }
  }
}

function createArchiveName(files: string[]) {
  const filesHash = crypto.createHash('md5').update(files.sort().join('&')).digest('hex');
  return `${new Date(savedState.lastUpdateTime).toISOString().slice(0, 10)}_${filesHash}.zip`;
}

async function assembleArchive(files: string[], outputFile: string) {
  const archive = archiver('zip', { zlib: { level: 0 } });
  const errorPromise = new Promise((_resolve, reject) => {
    archive.on('warning', (err) => reject(new Error(`Warning when creating archive:\n ${err.message}`)));
    archive.on('error', (err) => reject(new Error(`Error when creating archive:\n ${err.message}`)));
  });

  for (const filePath of files) {
    archive.file(filePath, { name: path.relative(userConfig.sourceDir, filePath) });
  }

  const output = createWriteStream(outputFile);
  try {
    archive.pipe(output);

    startProgressBar(files.length);
    archive.on('progress', (progress) => {
      updateProgressBar(progress.entries.processed, files[progress.entries.processed - 1]);
    });
    await Promise.race([errorPromise, archive.finalize()]);
  } finally {
    stopProgressBar();
    output.close();
  }
}

async function assembleArchiveSafely(files: string[], outputFile: string) {
  const tempArchivePath = path.resolve(path.dirname(outputFile), `.backupsync_temp_${path.basename(outputFile)}`);
  if (existsSync(tempArchivePath)) {
    throw Error(`Temp archive file already exists. Stop other processes or delete the file at "${tempArchivePath}".`);
  }
  try {
    await assembleArchive(files, tempArchivePath);
    await fs.rename(tempArchivePath, outputFile);
  } catch (err) {
    await fs.rm(tempArchivePath);
    throw err;
  }
}

export async function getFilesWaitingToBeArchived() {
  const fileItr = getFiles(path.resolve(userConfig.sourceDir));
  const fileList: string[] = [];
  for await (const filePath of fileItr) {
    if (path.basename(filePath).startsWith('.')) {
      continue;
    }
    const fileInfo = await fs.stat(filePath);
    const fileTime = Math.max(fileInfo.birthtimeMs, fileInfo.mtimeMs);
    if (fileTime >= savedState.lastUpdateTime) {
      fileList.push(filePath);
    }
  }
  return fileList;
}

export async function ensureArchive(fileList: string[]) {
  const archiveName = createArchiveName(fileList);
  const archivePath = path.resolve(userConfig.targetDir, archiveName);

  if (!existsSync(archivePath)) {
    console.log(`Archiving files into "${archiveName}"`);
    await assembleArchiveSafely(fileList, archivePath);
  } else {
    console.log(`Archive "${archiveName}" already exists locally`);
  }
  return archivePath;
}
