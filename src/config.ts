import { existsSync, readFileSync, writeFileSync } from 'fs';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import * as path from 'path';

const SAVED_STATE_FILE = '.backupstate.json';

export const UPLOAD_PART_SIZE = 100 * 1024 * 1024; // Size in MB

export enum S3StorageClass {
  STANDARD = 'STANDARD',
  DEEP_ARCHIVE = 'DEEP_ARCHIVE',
  GLACIER_IR = 'GLACIER_IR',
}

interface UserConfig {
  targetDir: string;
  sourceDir: string;
  bucketName: string;
  bucketRegion: string;
  bucketStorageClass: S3StorageClass;
}

let loadedUserConfig: UserConfig;
export function resolveCliArgs() {
  if (loadedUserConfig) {
    return loadedUserConfig;
  }
  const argv = yargs(hideBin(process.argv))
    .option('targetDir', { type: 'string', demandOption: true })
    .option('sourceDir', { type: 'string', demandOption: true })
    .option('bucketName', { type: 'string', demandOption: true })
    .option('bucketRegion', { type: 'string', demandOption: true })
    .option('bucketStorageClass', { type: 'string', choices: Object.keys(S3StorageClass), default: S3StorageClass.STANDARD })
    .config('config', function (configPath) {
      return JSON.parse(readFileSync(configPath, 'utf-8'));
    })
    .check((argv) => {
      if (!existsSync(argv.sourceDir)) {
        throw new Error('sourceDir does not exist.');
      }
      if (!existsSync(argv.targetDir)) {
        throw new Error('targetDir does not exist.');
      }
      if (!/\w{2}-\w+-\d/g.test(argv.bucketRegion)) {
        throw new Error('Invalid AWS region specified.');
      }
      return true;
    })
    .parseSync();

  loadedUserConfig = {
    targetDir: argv.targetDir,
    sourceDir: argv.sourceDir,
    bucketName: argv.bucketName,
    bucketRegion: argv.bucketRegion,
    bucketStorageClass: argv.bucketStorageClass,
  };
  return loadedUserConfig;
}

export const userConfig: Readonly<UserConfig> = new Proxy({} as UserConfig, {
  get(_t, name) {
    if (!loadedUserConfig) {
      throw new Error('Config not loaded');
    } else if (!(name in loadedUserConfig)) {
      throw new Error(`Invalid config key ${name.toString()}`);
    } else {
      return loadedUserConfig[name as keyof UserConfig];
    }
  },
});

interface SavedState {
  lastUpdateTime: number;
}

let loadedSavedState: SavedState;
function loadSavedState() {
  if (loadedSavedState) {
    return;
  }
  const savedStatePath = path.resolve(userConfig.targetDir, SAVED_STATE_FILE);
  loadedSavedState = existsSync(savedStatePath)
    ? JSON.parse(readFileSync(savedStatePath, 'utf-8'))
    : {
        lastUpdateTime: 0,
      };
}

function persistSavedState() {
  const savedStatePath = path.resolve(userConfig.targetDir, SAVED_STATE_FILE);
  writeFileSync(savedStatePath, JSON.stringify(loadedSavedState));
}

export const savedState: SavedState = new Proxy({} as SavedState, {
  get(_t, name) {
    loadSavedState();
    return loadedSavedState[name as keyof SavedState];
  },
  set(_t, name, value) {
    loadSavedState();
    switch (name) {
      case 'lastUpdateTime':
        loadedSavedState[name] = value;
        persistSavedState();
        return true;
      default:
        return false;
    }
  },
});
