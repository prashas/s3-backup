import * as fs from 'fs/promises';
import * as os from 'os';
import * as path from 'path';
import { ensureArchive, getFilesWaitingToBeArchived } from './archiver';

jest.mock('./config', () => ({
  userConfig: {
    sourceDir: path.join(os.tmpdir(), 's3synctest_archiver', 'input'),
    targetDir: path.join(os.tmpdir(), 's3synctest_archiver', 'output'),
  },
  savedState: {
    lastUpdateTime: 0,
  },
}));

const DEFAULT_TEST_DIR = path.join(os.tmpdir(), 's3synctest_archiver');

afterAll(async () => {
  await fs.rm(DEFAULT_TEST_DIR, { recursive: true, force: true });
});

beforeEach(async () => {
  await fs.rm(DEFAULT_TEST_DIR, { recursive: true, force: true });
  await fs.mkdir(DEFAULT_TEST_DIR);
  await fs.mkdir(path.join(DEFAULT_TEST_DIR, 'input'));
  await fs.mkdir(path.join(DEFAULT_TEST_DIR, 'output'));
});

async function createSampleFiles() {
  await fs.mkdir(path.join(DEFAULT_TEST_DIR, 'input', 'dir1'));
  await Promise.all([
    fs.writeFile(path.join(DEFAULT_TEST_DIR, 'input', 'test1.txt'), 'test1'),
    fs.writeFile(path.join(DEFAULT_TEST_DIR, 'input', 'test2.txt'), 'test2'),
    fs.writeFile(path.join(DEFAULT_TEST_DIR, 'input', '.ignorethisfile.txt'), 'ignorefile'),
    fs.writeFile(path.join(DEFAULT_TEST_DIR, 'input', 'dir1', 'somefile.txt'), 'somefile'),
  ]);
}

describe('getFilesWaitingToBeArchived()', () => {
  it('should be able to get the correct files to archive', async () => {
    await createSampleFiles();
    const files = await getFilesWaitingToBeArchived();
    expect(files.sort()).toEqual(
      ['test1.txt', 'test2.txt', path.join('dir1', 'somefile.txt')].map((v) => path.join(DEFAULT_TEST_DIR, 'input', v)).sort(),
    );
  });
});

describe('ensureArchive()', () => {
  it('should be able to archive all the files successfully', async () => {
    await createSampleFiles();
    const files = await getFilesWaitingToBeArchived();
    const archivePath = await ensureArchive(files);
    expect(archivePath).toContain(path.join(DEFAULT_TEST_DIR, 'output'));
    expect(path.basename(archivePath)).not.toContain('.backupsync_temp_');
  });

  it('should not leave a trace when it errors', async () => {
    await createSampleFiles();
    const files = [...(await getFilesWaitingToBeArchived()), path.join(DEFAULT_TEST_DIR, 'input', 'fakefile.txt')];
    await expect(ensureArchive(files)).rejects.toThrowError();
    const outputFiles = await fs.readdir(path.join(DEFAULT_TEST_DIR, 'output'));
    expect(outputFiles).toHaveLength(0);
  });
});
