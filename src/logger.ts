import * as cliProgress from 'cli-progress';

const bar = new cliProgress.SingleBar(
  {
    format: '[\u001b[32m{bar}\u001b[0m] {value}/{total} | ETA: {eta_formatted} {comment}',
    emptyOnZero: true,
    hideCursor: true,
    stopOnComplete: true,
  },
  cliProgress.Presets.shades_grey,
);

export function startProgressBar(total: number, startValue = 0) {
  bar.start(total, startValue, { comment: '' });
}

export function updateProgressBar(progress: number, comment?: string) {
  bar.update(progress, { comment: comment ? `| ${comment}` : '' });
}

export function updateProgressWithUnit(progress: number, totalSizeInUnits: number, unit: string) {
  const comment = `${Math.round((progress / bar.getTotal()) * totalSizeInUnits)}${unit}/${totalSizeInUnits}MB`;
  updateProgressBar(progress, comment);
}

export function stopProgressBar() {
  updateProgressBar(bar.getTotal());
  bar.stop();
}
