import * as path from 'path';
import { doesObjectExist, uploadToGlacier } from './s3';
import { resolveCliArgs, userConfig, savedState } from './config';
import { ensureArchive, getFilesWaitingToBeArchived } from './archiver';

async function run() {
  resolveCliArgs();

  const updateStartTime = Date.now();

  console.log(`Running on ${userConfig.sourceDir}`);
  const fileList = await getFilesWaitingToBeArchived();

  if (fileList.length === 0) {
    console.log('No files to archive!');
    process.exit(0);
  } else {
    console.log(fileList.length, 'files need to be updated');
  }

  const archivePath = await ensureArchive(fileList);

  const s3Key = path.relative(userConfig.targetDir, archivePath);
  if (await doesObjectExist(s3Key)) {
    console.log('Already uploaded to S3');
  } else {
    await uploadToGlacier(archivePath, s3Key);
  }

  savedState.lastUpdateTime = updateStartTime;
  process.exit(0);
}

run();
